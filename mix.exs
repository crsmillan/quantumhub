defmodule Quantum.MixProject do
  use Mix.Project
  require Logger

  def project do
    [
      apps_path: "apps",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()     
    ]
  end

  defp deps do
    [
      {:distillery, "~> 2.0"}
    ]
  end

  defp aliases do
    if Mix.env == :dev do
      [
        reset_data: [
          "ecto.drop", 
          "ecto.create", 
          "ecto.migrate", 
          "ecto.migrations", 
          "event_store.drop", 
          "event_store.create", 
          "event_store.init",
        ],
        reset_stellar_db: [
          "ecto.drop -r Stellar.Bridge.Repo",
          "ecto.create -r Stellar.Bridge.Repo",
        ]
      ]
    
    else
      []
    end
  end

end
