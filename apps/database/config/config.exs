use Mix.Config

config :database, Stellar.Bridge.Repo,
  database: "stellar_bridge",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"


config :database, Accounting.Repo,
  database: "accounting_database",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"


config :database, 
  ecto_repos: [ Accounting.Repo]#, Stellar.Bridge.Repo]


