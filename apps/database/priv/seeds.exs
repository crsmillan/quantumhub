alias Accounting.Repo
alias Accounting.Schemas.{Accounts}


%Accounts{
  account_id: "540348c6-af3a-47d8-b78b-47e1f2ad4811",
  active: true,
  balance: 10000.00,
  type: "SEED -type",
}|> Repo.insert!


%Accounts{
  account_id: "1267aa4b-f2ce-4eb5-bbc8-986be6a242c5",
  active: true,
  balance: 10000.00,
  type: "SEED -type",
}|> Repo.insert!
