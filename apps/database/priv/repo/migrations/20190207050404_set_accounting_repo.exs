defmodule Accounting.Repo.Migrations.SetAccountingRepo do
  use Ecto.Migration

  def change do
    create table(:partners, primary_key: false) do
      add :partner_id,       :uuid,              primary_key: true
      add :bic,              :string
      add :name,             :string
    end

    create table(:users, primary_key: false) do
      add :user_id,         :uuid,               primary_key: true
      add :name,            :string
      add :phone_number,    :string
      add :email,           :string
      add :partner_id,      references(:partners, [column: :partner_id, type: :uuid])
    end

    create table(:accounts, primary_key: false) do
      add :account_id,       :uuid,             primary_key: true
      add :assigment_id,     :string
      add :balance,          :float
      add :active,           :boolean,          default: false
      add :compliance,       :boolean,          default: false
      add :type,             :string
      add :status,           :string
      add :user_id,          references(:users,    [column: :user_id,    type: :uuid])
      add :partner_id,       references(:partners, [column: :partner_id, type: :uuid])
    end

    # Create Indixes 
    # UUIDs not false


    create unique_index(:partners, [:partner_id])
    create unique_index(:users,    [:user_id])
    create unique_index(:accounts, [:account_id])
  end
end
