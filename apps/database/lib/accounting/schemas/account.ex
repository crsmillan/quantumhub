defmodule Accounting.Schemas.Accounts do
  alias Accounting.Schemas.{Users, Partners, Accounts}
  alias Accounting.Repo
  use Ecto.Schema
  
  @primary_key {:account_id, :binary_id, autogenerate: false}
  @foreign_key_type :binary_id

  schema "accounts" do
    field :balance,         :float
    field :assigment_id,    :string
    field :active,          :boolean
    field :compliance,      :boolean
    field :type,            :string
    field :status,          :string
    belongs_to :user,       Users     , [references: :user_id]
    belongs_to :partner,    Partners  , [references: :partner_id]
  end

  def get(id) when is_binary(id) do
    case Repo.get(Accounts, id) do
      nil ->  {:error, :account_not_found}
      acc ->  {:ok, acc}
    end
  end
 
end