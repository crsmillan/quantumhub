defmodule Accounting.Schemas.Users do
  alias Accounting.Repo
  alias Accounting.Schemas.{Accounts, Partners, Users}
  use Ecto.Schema

  @primary_key {:user_id, :binary_id, autogenerate: false}
  @foreign_key_type :binary_id

  schema "users" do
    field       :name,         :string
    field       :phone_number, :string
    field       :email,        :string
    has_many    :accounts, Accounts ,  [foreign_key: :user_id]
    belongs_to  :partner,  Partners ,  [references: :partner_id]
  end

  def get(user_id) when is_binary(user_id) do
    case Repo.get(Users, user_id) do
      nil -> {:error, :user_not_found}
      user -> {:ok, user}
    end
  end

end