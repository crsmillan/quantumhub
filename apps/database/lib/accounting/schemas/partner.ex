defmodule Accounting.Schemas.Partners do
  alias Accounting.Schemas.{Users, Accounts, Partners}
  alias Accounting.Repo
  use Ecto.Schema
  # import Ecto.Changeset
  
  @primary_key {:partner_id, :binary_id, autogenerate: false}
  @foreign_key_type :binary_id

  schema "partners" do
    field     :bic,     :string
    field     :name,    :string
    has_many  :users,   Users     , [foreign_key: :partner_id]  
    has_one   :account, Accounts  , [foreign_key: :partner_id]
  end

  def get(partner_id) when is_binary(partner_id) do
    case Repo.get(Partners, partner_id) do
      nil -> {:error, :partner_not_found}
      partner -> {:ok, partner}
    end
  end

end