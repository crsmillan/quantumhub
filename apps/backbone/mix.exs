defmodule Backbone.MixProject do
  use Mix.Project

  def project do
    [
      app: :backbone,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [
        :logger,
        :eventstore
      ],
      
      mod: {Backbone.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [ 
      {:database, in_umbrella: true},
      {:jason, "~> 1.1"},
      {:commanded, "~> 0.18"},
      {:eventstore, "~> 0.16"},
      {:commanded_eventstore_adapter, "~> 0.5"},
      {:commanded_ecto_projections, "~> 0.8"},
      {:nanoid, "~> 1.0.1"},
    ]
  end
end
