use Mix.Config

config :commanded, event_store_adapter: Commanded.EventStore.Adapters.EventStore

config :eventstore, EventStore.Storage,
  serializer: Commanded.Serialization.JsonSerializer, 
  username: "postgres",
  password: "postgres",
  database: "eventstore_database",
  hostname: "localhost",
  pool_size: 10,
  pool_overview: 2
