defmodule Backbone.Server do
  require Logger
  use GenServer
  @server :backbone_server

  alias Backbone.Router
  alias Backbone.Commands.{
    OpenAccount,
    DeleteAccount,
    Deposit,
    Withdraw,
    Send
  }

  # Client API
  def start_link(opts \\ %{}) do
    GenServer.start_link(__MODULE__, opts, name: @server)
  end

  def stop, do:  GenServer.cast(@server, :stop)

  # Callbacks
  def init(args) do
    Logger.debug "Backbone Server Started on -> (#{inspect self()})"
    {:ok, args}
  end

  def handle_info(msg, state) do
    Logger.info "BackboneServe Message: #{inspect msg}"
    {:no_reply, state}
  end

  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end

  def terminate(reason, state) do
    Logger.error "Backbone Server Ended --> #{inspect reason}"
    inspect state
    :ok
  end

  def handle_cast({:delete_account, account}, state) do
    with {:ok, acc} <- Accounting.Schemas.Accounts.get(account) do
      %DeleteAccount{account_id: acc.account_id}
      |> Router.dispatch
    else
      _ -> raise RuntimeError
    end

    {:noreply, state}
  end

  def handle_cast({:open_account, attrs}, state) do
    %OpenAccount{
      user_id:    attrs.user_id,
      account_id: UUID.uuid4, 
      type:       "backbone-type"
    }
    |> Router.dispatch
    |> case do
      :ok ->  Logger.info :created_successfully 
      {:error, reason} ->  Logger.error reason
    end

    {:noreply, state}
  end

  def handle_cast({:deposit, params}, state) do
    %Deposit{account_id: params.account_id, amount: params.amount} 
    |> Router.dispatch
    |> case do
      :ok ->    Logger.info :amount_deposited
      {:error, reason} -> Logger.error reason
    end
    {:noreply, state}
  end

  def handle_cast({:withdraw, params}, state) do
    %Withdraw{account_id: params.account_id, amount: params.amount}   
    |> Router.dispatch
    |> case do
      :ok -> Logger.info :amount_withdrawn
      {:error, err} -> Logger.error err
    end
    {:noreply, state}
  end

  def handle_cast({:transfer, params}, state) do
    %Send{
      account_id: params.from, 
      amount:     params.amount, 
      to:         params.to, 
      transfer_id: Nanoid.generate()
    }
    |> Router.dispatch
    |> case do
      :ok -> Logger.info :transfer_successfully_done
      {:error, err} -> Logger.error err
    end

    {:noreply, state}
  end


  
  
end