defmodule Backbone.Helpers.OwnerBypass do
  alias Accounting.Schemas.{Partners, Users}

  def select_ownership(%{partner_id: partner, user_id: user}) do
    cond do
      is_binary(partner) and is_nil(user)    -> {:partner, partner}
      is_binary(user)    and is_nil(partner) -> {:user,    user}
      is_binary(partner) and is_binary(user) -> :error

      true -> :error
    end
  end

  def get_owner({owner, id}) do
    cond do
      owner == :partner -> Partners.get(id)
      owner == :user ->    Users.get(id)
      true -> :error
    end
  end

  def build_assoc({:ok, owner}, %{} = account) do
    case owner do
      %Partners{} -> Ecto.build_assoc(owner, :account,  account)
      %Users{}    -> Ecto.build_assoc(owner, :accounts, account)
    end
  end


end
