defmodule Backbone.Helpers.SchemaVerifier do
  alias Accounting.Repo
  alias Accounting.Schemas.{Partners, Users, Accounts}

  def exist?(schema, id) when is_atom(schema) and is_binary(id) do
    case schema do
      :partner -> verify(Partners, id)
      :user ->    verify(Users, id)
      :account -> verify(Accounts, id)
      _-> raise ArgumentError, "Try -> :partner, :user, :account"
    end
  end
  

  defp verify(schema, id) do
    case Repo.get(schema, id) do
      nil    -> false
      _strct -> true
    end
  end
end