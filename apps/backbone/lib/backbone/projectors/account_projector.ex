defmodule Backbone.Projectors.Account do
  alias Backbone.Helpers.OwnerBypass
  alias Ecto.Multi
  alias Backbone.Events.{AccountOpened, AccountDeleted}
  alias Accounting.Schemas.{Accounts}

  use Commanded.Projections.Ecto, 
  name: "AccountProjector", 
  repo: Accounting.Repo
  
  # OpenAccount
  project %AccountOpened{} = account, _metadata, fn  multi ->
    identities = %{partner_id: account.partner_id, user_id: account.user_id}
    with {owner, id} <- OwnerBypass.select_ownership(identities) do
      Multi.insert(
        multi, 
        :open_account, 
        OwnerBypass.get_owner({owner, id}) 
        |> OwnerBypass.build_assoc(account)
      )
    end
  end
  
  # DeleteAnAccount
  project %AccountDeleted{account_id: account_id}, _metadata, fn multi -> 
    Multi.delete(multi, :delete_account, %Accounts{account_id: account_id})      
  end


end