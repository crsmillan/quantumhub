defmodule Backbone.Projectors.Transaction do
  alias Backbone.Events.{Deposited, Withdrawn, Sent, Received}
  alias Accounting.Schemas.Accounts

  use Commanded.Projections.Ecto, 
  name: "TransactionProjector", 
  repo: Accounting.Repo
  
  # Deposited 
  project %Deposited{} = evt, _metadata, fn multi -> 
    banking(multi, evt.account_id, evt.amount) 
  end

  # Withdrawn
  project %Withdrawn{} = evt, _metadata, fn multi -> 
    banking(multi, evt.account_id, -evt.amount) 
  end

  # Transfer_Sent
  project %Sent{} = evt, _metadata, fn multi      -> 
    banking(multi, evt.account_id, -evt.amount) 
  end

  # Transfer_Received
  project %Received{} = evt, _metadata, fn multi  ->  
    banking(multi, evt.account_id, evt.amount) 
  end

  defp banking(multi, account_id, amount) do
    Ecto.Multi.insert(
      multi,
      :banking,
      %Accounts{
        account_id: account_id,
        balance:    amount
      },
      conflict_target:  :account_id,
      on_conflict:      [inc: [balance: amount]]
    )
  end
end