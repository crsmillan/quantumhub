defmodule Backbone.Events do
  defmodule AccountOpened  do 
    @derive Jason.Encoder 
    defstruct [:account_id, :balance, :active, :type, :compliance, :user_id, :partner_id, :status]
  end
  
  defmodule AccountDeleted do 
    @derive Jason.Encoder 
    defstruct [:account_id]
  end
  
  defmodule Deposited do 
    @derive Jason.Encoder 
    defstruct [:account_id, :amount]
  end
  
  defmodule Withdrawn do 
    @derive Jason.Encoder 
    defstruct [:account_id, :amount]
  end
  
  defmodule Sent do 
    @derive Jason.Encoder 
    defstruct [:account_id, :amount, :transfer_id, :to]
  end
  
  defmodule Received do 
    @derive Jason.Encoder 
    defstruct [:account_id, :amount, :transfer_id, :from]
  end
end
