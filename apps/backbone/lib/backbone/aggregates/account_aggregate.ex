defmodule Backbone.Aggregates.Account do
  @moduledoc """ 
    Account Aggegate Business Logic
  """
  alias Backbone.Helpers.{SchemaVerifier, OwnerBypass}
  alias Backbone.Aggregates.Account
  alias Backbone.Commands.{
    OpenAccount, 
    DeleteAccount,  
    Deposit, 
    Withdraw, 
    Send, 
    Receive,
  }
  alias Backbone.Events.{
    AccountOpened, 
    AccountDeleted, 
    Deposited, 
    Withdrawn,
    Sent, 
    Received,
  }
  
  defstruct [
    account_id:   nil, 
    user_id:      nil,
    partner_id:   nil,
    balance:      nil, 
    active:       nil, 
    type:         nil, 
    compliance:   nil,
    status:       nil,
  ]

  
  # # API
  # OpenAccount for user
  def execute(%Account{account_id: nil, user_id: nil}, %OpenAccount{user_id: user_id, partner_id: partner_id} = account) when is_binary(user_id) or is_binary(partner_id) do
    identities = %{partner_id: partner_id, user_id: user_id}
    
    with {owner, id} <- OwnerBypass.select_ownership(identities) do
      case SchemaVerifier.exist?(owner, id) do
        true  -> account_opened(account)
        false -> {:error, :"#{owner}_not_found"} 
      end
    else
      :error -> {:error, :invalid_params_please_verify}
    end
  end
  
  # OpenAccount error, user_id or partner_id can't be nil
  def execute(%Account{}, %OpenAccount{user_id: user_id, partner_id: partner_id}) when is_nil(user_id) and is_nil(partner_id) do
    {:error, :ownership_is_missing}
  end
  
  # OpenAccount_Error 
  def execute(%Account{}, %OpenAccount{}) do
    {:error, :account_already_in_use}
  end
  
  # DeleteAccount
  def execute(_, %DeleteAccount{account_id: account_id}) do
    %AccountDeleted{account_id: account_id}
  end
  
  # InactiveAccount_Error
  def execute(%Account{active: active}, _) when active != true do
    {:error, :invalid_or_non_active_account}
  end
  
  # Deposit_amout_nil_error
  def execute(_, %Deposit{amount: amount}) when amount == nil do
    {:error, :amount_cannot_be_nil}
  end
  
  # Deposit error, amount can not be Zero
  def execute(_, %Deposit{amount: amount}) when amount == 0 do
    {:error, :amount_equals_zero}
  end
  
  # Deposit error, amount can not be negative
  def execute(_, %Deposit{amount: amount}) when amount < 0 do
    {:error, :amount_is_negative}
  end
  
  # Deposit
  def execute(_, %Deposit{account_id: account_id, amount: amount})  
  when is_float(amount) and amount != 0 do
    %Deposited{account_id: account_id, amount: amount}
  end
  
  # Deposit error, Amount must be float
  def execute(_, %Deposit{amount: amount}) when amount != Float do
    {:error, :amount_must_be_float}
  end
  
  # Deposit_error
  def execute(%Account{active: active}, %Deposit{}) when active == false do
    {:error, :active_your_account}
  end
  
  # Withdraw error, amount can not be negative
  def execute(_, %Withdraw{amount: amount}) when amount < 0 do
    {:error, :amount_is_negative}
  end
  
  # Withdraw error, amount can not be zero
  def execute(_, %Withdraw{ amount: amount}) when amount == 0 do
    {:error, :amount_equals_zero}
  end
  
  # Withdraw
  def execute(%Account{balance: balance}, %Withdraw{account_id: account_id, amount: amount})
  when is_float(amount)  and balance >= amount do
    %Withdrawn{account_id: account_id, amount: amount}  
  end
  
  # Withdraw_error
  def execute(%Account{balance: balance}, %Withdraw{amount: amount}) 
  when balance < amount do
    {:error, :insufficient_funds}
  end
  
  # Withdraw error, Amount must be float
  def execute(_, %Withdraw{amount: amount}) when amount != FLoat do
    {:error, :amount_must_be_float}
  end
  
  # Transfer_Send_Error
  def execute(%Account{balance: balance}, %Send{amount: amount}) 
  when is_float(amount) and balance < amount do
    {:error, :insufficient_funds}    
  end
  
  # Transfer_Send
  def execute(_, %Send{amount: amount} = send) when is_float(amount) do
    %Sent{
      account_id:   send.account_id, 
      amount:       send.amount, 
      transfer_id:  send.transfer_id, 
      to:           send.to
    }
  end
  
  # Transfer Send err, :amount_must_be_float
  def execute(_, %Send{amount: amount}) when amount != Float do
    {:error, :amount_must_be_float}
  end
  
  # Transfer_Receive
  def execute(_, %Receive{} = transfer) do
    %Received{
      account_id:   transfer.account_id,
      amount:       transfer.amount, 
      transfer_id:  transfer.transfer_id, 
      from:         transfer.from
    }
  end
  
  # # Helpers
  # account_opened event handler
  defp account_opened(%OpenAccount{} = event )do
    %AccountOpened{
      account_id:   event.account_id, 
      user_id:      event.user_id,
      partner_id:   event.partner_id,
      balance:      0.0, 
      active:       true, 
      compliance:   false,
      type:         event.type, 
      status:       event.status,
    }
  end

  # # State Mutators
  # AccountOpened
  def apply(%Account{} = account, %AccountOpened{} = event) do
    %Account{
      account | 
      account_id:   event.account_id, 
      user_id:      event.user_id,
      partner_id:   event.partner_id,
      active:       event.active, 
      balance:      event.balance,
      compliance:   event.compliance,
      type:         event.type,
      status:       event.status,
    } 
  end
  
  # AccountDeleted
  def apply(%Account{} = account, %AccountDeleted{account_id: id}) do
    %Account{
      account | 
      account_id: id
    }
  end

  # Deposited
  def apply(%Account{} = account, %Deposited{amount: amount}) do 
    %Account{
      account | 
      balance: account.balance + amount
    }
  end

  # Withdrawn
  def apply(%Account{} = account, %Withdrawn{amount: amount}) do
    %Account{ 
      account | 
      balance: account.balance - amount
    }
  end

  # Transfer_Sent
  def apply(%Account{} = account, %Sent{amount: amount}) do
    %Account{
      account | 
      balance: account.balance - amount 
    }
  end

  # Transfer_Received
  def apply(%Account{} = account, %Received{amount: amount}) do
    %Account{
      account | 
      balance: account.balance + amount
    }
  end


end