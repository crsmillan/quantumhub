defmodule Backbone.Router do
  alias Backbone.Aggregates.Account 
  use Commanded.Commands.Router

  alias Backbone.Commands.{
    OpenAccount,
    DeleteAccount,
    Deposit,
    Withdraw,
    Send,
    Receive
  }
  

  dispatch [
    OpenAccount,
    DeleteAccount,
    Deposit,
    Withdraw,
    Send,
    Receive
  ], 
  to: Account, 
  identity: :account_id
end