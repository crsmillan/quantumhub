defmodule Backbone.ProcessManager.Transfer do
  alias Backbone.Events.{Sent, Received}
  alias Backbone.Commands.{Receive}

  use Commanded.ProcessManagers.ProcessManager,
  name: "Transfer ProcessManager",
  router:  Backbone.Router,
  event_timeout: :timer.minutes(10)

  @derive Jason.Encoder
  defstruct [:from, :amount, :to, :transfer_id,]

  def interested?(%Sent{transfer_id: tid}),     do: {:start,  tid}
  def interested?(%Received{transfer_id: tid}), do: {:stop,   tid}
  def interested?(_event), do: false
  
  def handle(_, %Sent{account_id: from, amount: amount, transfer_id: tid, to: account_id}) do
    %Receive{
      account_id:   account_id, 
      amount:       amount, 
      transfer_id:  tid, 
      from:         from
    }
  end

  def apply(transfer, %Sent{amount: amount, transfer_id: tid, to: to, account_id: from}) do
    %Backbone.ProcessManager.Transfer{
      transfer |
        from:         from,
        amount:       amount,
        to:           to,
        transfer_id:  tid
    }
  end

  # Stop process manager after three failures
  def error({:error, _failure}, _failed_message, %{context: %{failures: failures}})
    when failures >= 3
  do
    {:stop, :too_many_failures}
  end

  # Retry command, record failure count in context map
  def error({:error, _failure}, _failed_message, %{context: context}) do
    context = Map.update(context, :failures, 1, fn failures -> failures + 1 end)
    {:retry, context}
  end
end
