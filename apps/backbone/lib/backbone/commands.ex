defmodule Backbone.Commands do
  defmodule OpenAccount   do 
    @enforce_keys [:account_id]
    defstruct [:account_id, :balance, :active, :type, :compliance, :user_id, :partner_id, :status]
  end

  defmodule DeleteAccount do 
    @enforce_keys [:account_id]
    defstruct [:account_id]
  end

  defmodule Deposit       do 
    @enforce_keys [:account_id]
    defstruct [:account_id, :amount]
  end

  defmodule Withdraw      do 
    @enforce_keys [:account_id]
    defstruct [:account_id, :amount]
  end

  defmodule Send          do 
    @enforce_keys [:account_id]
    defstruct [:account_id, :amount, :transfer_id, :to]
  end

  defmodule Receive       do 
    @enforce_keys [:account_id]
    defstruct [:account_id, :amount, :transfer_id, :from]
  end

end