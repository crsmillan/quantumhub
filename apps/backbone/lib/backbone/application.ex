defmodule Backbone.Application do
  @moduledoc false

  use Application
  alias Backbone.Projectors.{Account, Transaction}
  alias Remittable.Projectors.{Partner, User}
  alias Backbone.ProcessManager.{Transfer}

  def start(_type, _args) do
    children = [
      {Account,         []},
      {Transaction,     []},
      {Transfer,        []},
      {Partner,         []},
      {User,            []},
      {Backbone.Server, []}
    ]

    opts = [strategy: :one_for_one, name: Backbone.Supervisor]
    Supervisor.start_link(children, opts)
  end
end


