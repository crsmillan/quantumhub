defmodule Backbone.JsonDecoder do
  alias Backbone.Events.{
    AccountOpened,
    AccountDeleted,
    Deposited,
    Withdrawn,
    Sent,
    Received,
  }
  alias Backbone.ProcessManager.{Transfer}

  defimpl Commanded.Serialization.JsonDecoder, 
  for: [AccountOpened, AccountDeleted, Deposited, Withdrawn, Sent,Received,Transfer] do
    def decode(event) do
      event
    end
  end
end
