defmodule Remittable.Router do
  use Commanded.Commands.Router
  alias Remittable.Aggregates.{User, Partner}
  alias Backbone.Aggregates.{Account}

  alias Remittable.Commands.{
    CreateUser,
    CreatePartner,
  }
  
  alias Backbone.Commands.{
    OpenAccount
  }

  dispatch [
    CreateUser,
  ],
  to: User,
  identity: :user_id

  
  dispatch [
    CreatePartner
  ],
  to: Partner,
  identity: :partner_id

  dispatch [OpenAccount],
  to: Account,
  identity: :account_id

end