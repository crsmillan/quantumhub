defmodule Remittable.Events do
  defmodule UserCreated do 
    @derive Jason.Encoder
    defstruct [
      :user_id,  
      :name, 
      :partner_id,
      :phone_number,
      :email,
    ]
  end

  defmodule PartnerCreated  do 
    @derive Jason.Encoder
    defstruct [:partner_id, :name,  :bic]
  end
  
end