defmodule Remittable.Commands do
  defmodule CreateUser     do 
    @enforce_keys [:user_id]
    defstruct [
      :user_id, 
      :name, 
      :partner_id,
      :phone_number,
      :email,
    ]
  end

  defmodule CreatePartner  do 
    @enforce_keys [:partner_id]
    defstruct [:partner_id, :name, :bic]
  end

end