defmodule Remittable.JsonDecoder do
  require Logger
  
  alias Remittable.Events.{
    UserCreated,
    PartnerCreated,
    AccountAssigned,
  }

  alias Remittable.ProcessManager.{AccountAssigment}

  defimpl Commanded.Serialization.JsonDecoder, 
  for: [UserCreated, PartnerCreated, AccountAssigned, AccountAssigment]  do
    def decode(event) do 
    #  Logger.info "#{inspect event}"
     event
    end
  end

end
