defmodule Remittable.Projectors.Partner do
  alias Remittable.Events.PartnerCreated
  alias Accounting.Schemas.Partners
  use Commanded.Projections.Ecto,
  name: "PartnerProjector",
  repo: Accounting.Repo

  project %PartnerCreated{} = partner, _metadata, fn multi -> 
    Ecto.Multi.insert(
      multi, 
      :new_partner,
      %Partners{
        partner_id: partner.partner_id,
        bic:        partner.bic,
        name:       partner.name,
      }
    )
  end
  
end