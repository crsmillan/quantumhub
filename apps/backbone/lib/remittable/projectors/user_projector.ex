defmodule Remittable.Projectors.User do
  alias Ecto.Multi
  alias Remittable.Events.UserCreated
  alias Accounting.Schemas.{Partners}
  use Commanded.Projections.Ecto, 
  name: "UserProjector", 
  repo: Accounting.Repo
  
  # CreateUser
  project %UserCreated{} = user, _metadata, fn multi ->
    {:ok, partner} = Partners.get(user.partner_id) 
    Multi.insert(multi, :new_user,  Ecto.build_assoc(partner, :users, user))
  end

  # DeleteUser


end
