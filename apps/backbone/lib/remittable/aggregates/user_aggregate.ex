defmodule Remittable.Aggregates.User do
  alias Backbone.Helpers.SchemaVerifier
  alias Accounting.Schemas.Partners
  alias Remittable.Aggregates.User
  alias Remittable.Commands.{CreateUser}
  alias Remittable.Events.{UserCreated}
  
  defstruct [
    user_id:      nil, 
    name:         nil, 
    partner_id:   nil,
    phone_number: nil,
    email:        nil,
  ]


  # # API
  # CreateUser error, :partnerID_is_missing
  def execute(%User{}, %CreateUser{partner_id: partner_id}) when is_nil(partner_id) do
    {:error, :partnerID_is_missing}
  end
  
  # CreateUser error, :invalid_params_please_verify
  def execute(%User{}, %CreateUser{partner_id: pid, user_id: uid}) when is_nil(pid) and is_nil(uid) do
    {:error, :invalid_params_please_verify}
  end

  # CreateUser
  def execute(%User{user_id: id}, %CreateUser{} = user)  when is_nil(id)  do
    with true <- SchemaVerifier.exist?(:partner, user.partner_id) do
      %UserCreated{
        user_id:        user.user_id, 
        name:           user.name, 
        partner_id:     user.partner_id,
        phone_number:   user.phone_number,
        email:          user.email,
      }
    else
      false -> {:error, :invalid_partnerID}
    end

  end

 # CreateUser error,
  def execute(%User{}, %CreateUser{})do
    {:error, :user_already_in_use}
  end

  
  
  # # StateMutators
  # UserCreated
  def apply(user, %UserCreated{} = new) do
    %User{
      user |
        user_id:        new.user_id, 
        name:           new.name, 
        partner_id:     new.partner_id,
        phone_number:   new.phone_number,
        email:          new.email
    }
  end

end