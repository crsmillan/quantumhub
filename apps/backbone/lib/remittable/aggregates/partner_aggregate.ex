defmodule Remittable.Aggregates.Partner do
  alias Remittable.Aggregates.Partner
  alias Remittable.Commands.{CreatePartner}
  alias Remittable.Events.{PartnerCreated}
  
  defstruct [
    partner_id: nil, 
    name:       nil, 
    bic:        nil,
  ]


  # # API
  # CreatePartner
  def execute(%Partner{partner_id: nil}, %CreatePartner{} = partner) do
    %PartnerCreated{
      partner_id: partner.partner_id,
      name:       partner.name,
      bic:        partner.bic
    }
  end
  
  # Create Partner error, 
  def execute(%Partner{}, %CreatePartner{}) do
    {:error, :partner_already_in_use}
  end

  # # StateMutators
  # CreatePartner
  def apply(partner, %PartnerCreated{} = new) do
    %Partner{
      partner |
        partner_id: new.partner_id, 
        name:       new.name, 
        bic:        new.bic
    }
  end
end