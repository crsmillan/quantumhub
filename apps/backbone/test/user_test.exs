defmodule Remittable.UserTest do
  use ExUnit.Case, async: true
  doctest Remittable.Aggregates.User

  import Commanded.Assertions.EventAssertions
  alias Remittable.Router
  alias Remittable.Commands.{CreateUser}
  alias Remittable.Events.{UserCreated}

  @user_id "8ffab761-50d8-4457-b9a4-878cb87ffada"
  @partner_id "1ec5f052-f69b-4f4f-bc40-b0a93b47e334"
  @phone_number "477 158 5879"

  test "Create user error, user_already_in_use" do
    assert Router.dispatch(%CreateUser{partner_id: @partner_id, user_id: @user_id}) == {:error, :user_already_in_use}
  end

  test "Create User" do
    id = UUID.uuid4
    :ok = Router.dispatch(%CreateUser{user_id: id, partner_id: @partner_id, phone_number: @phone_number})
    wait_for_event(UserCreated, &(&1.user_id == id and &1.phone_number == @phone_number))
  end

  test "Create User linked to a Partner" do
    id = UUID.uuid4
    :ok = Router.dispatch(%CreateUser{user_id: id, partner_id: @partner_id})
    wait_for_event(UserCreated, &(&1.user_id == id))
    wait_for_event(UserCreated, &(&1.partner_id == @partner_id))

    
  end

  test "Create User error, partnerID_is_missing" do
    assert Router.dispatch %CreateUser{user_id: "FAFE USER ID"} == {:error, :partnerId_is_missing}
  end

  test "CreateUser error, :invalid_params_please_verify" do
    assert Router.dispatch %CreateUser{partner_id: nil, user_id: nil} == {:error, :invalid_params_please_verify}
  end

  test "Create User, Verify that PartnerId exist" do
    alpha = Router.dispatch %CreateUser{
      partner_id: UUID.uuid4,
      user_id: UUID.uuid4
    }
    assert alpha == {:error, :invalid_partnerID}
  end


end

