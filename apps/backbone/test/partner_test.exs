defmodule Remittable.PartnerTest do
  use ExUnit.Case, async: true
  doctest Remittable.Aggregates.Partner

  import Commanded.Assertions.EventAssertions
  alias Remittable.Router
  alias Remittable.Commands.{CreatePartner}
  alias Remittable.Events.{PartnerCreated}


  test "Create Parnership" do
    id = UUID.uuid4
    :ok = Router.dispatch(%CreatePartner{partner_id: id})
    wait_for_event(PartnerCreated, &(&1.partner_id == id))
  end

  test "Create Partnership error" do
    partner = Router.dispatch %CreatePartner{partner_id: nil}
    refute partner == :ok
    assert partner == {:error, :invalid_aggregate_identity}
  end

  test "" do
    refute false
  end
end