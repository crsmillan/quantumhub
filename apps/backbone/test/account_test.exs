defmodule Backbone.AccountTest do
  use ExUnit.Case, async: true 
  doctest Backbone.Aggregates.Account

  import Commanded.Assertions.EventAssertions
  alias Backbone.Router
  alias Backbone.Commands.{OpenAccount,Deposit,Withdraw,Send,Receive,}
  alias Backbone.Events.{Deposited,Withdrawn,Sent,Received}
  
  @id1   "540348c6-af3a-47d8-b78b-47e1f2ad4811"
  @id2  "1267aa4b-f2ce-4eb5-bbc8-986be6a242c5"
  @user_id "d1e9a67b-aea4-4da2-8ad8-c7117fe819ab"
  @partner_id "1ec5f052-f69b-4f4f-bc40-b0a93b47e334"

  setup_all do
    # create accounts
    acc1 = Router.dispatch %OpenAccount{account_id: @id1 ,type: "test-type", user_id: @user_id} 
    acc2 = Router.dispatch %OpenAccount{account_id: @id2 ,type: "test-type", user_id: @user_id}
    # deposit
    Router.dispatch %Deposit{account_id: @id1, amount: 1_000.00}
    Router.dispatch %Deposit{account_id: @id2, amount: 1_000.00}
    # context
    %{
      acc1: acc1,
      acc2: acc2
    }
  end
  
  test "Account alredy in use error", context do
    error  = {:error, :account_already_in_use}
    assert context.acc1 == error
    assert context.acc2 == error
  end

  test "An account can't open without linking an user or partner" do
    assert Router.dispatch(%OpenAccount{account_id: UUID.uuid1}) == {:error, :ownership_is_missing}
  end
  
  test "OpenAccount OwnershipBypass" do
    assert {:error, :user_not_found} == Router.dispatch %OpenAccount{account_id: UUID.uuid4, user_id: UUID.uuid4}   
    assert {:error, :partner_not_found} == Router.dispatch %OpenAccount{account_id: UUID.uuid4, partner_id: UUID.uuid4}
    assert {:error, :ownership_is_missing} == Router.dispatch(%OpenAccount{account_id: UUID.uuid1})
    
    assert Router.dispatch(%OpenAccount{account_id: UUID.uuid1, partner_id: @partner_id}) == :ok
    assert Router.dispatch(%OpenAccount{account_id: UUID.uuid1, user_id:    @user_id}) == :ok

  end
  
  test "Account can not belongs to more than uniq identitie" do
    assert Router.dispatch %OpenAccount{account_id: UUID.uuid4, user_id: UUID.uuid4, partner_id: UUID.uuid4 } == {:error, :invalid_params_please_verify}
  end

  test "OpenAccount Undefined single function" do
    assert_raise(UndefinedFunctionError, &Backbone.Server.openAccount/0) 
  end

  test "Invalid Account" do 
    assert Router.dispatch(%Deposit{account_id: "Invalid_ID_001"}) == {:error, :invalid_or_non_active_account}
  end

  test "Desposit amount can't be nil" do
    assert {:error, :amount_cannot_be_nil} == Router.dispatch(%Deposit{account_id: @id1})
  end

  test "Deposit amount can't be zero" do
    assert {:error, :amount_equals_zero} == Router.dispatch(%Deposit{account_id: @id1, amount: 0000})
  end

  test "Deposit amount can't be negative" do
    assert {:error, :amount_is_negative} == Router.dispatch(%Deposit{account_id: @id1, amount: -500})
  end

  test "Deposit 001" do 
    :ok = Router.dispatch(%Deposit{account_id: @id1, amount: 500/1})
    # wait_for_event(Deposited, fn deposited -> deposited.amount == 500.00 end)
    wait_for_event(Deposited, &(&1.amount == 500.00))
  end
  
  test "Deposit 002" do 
    :ok = Router.dispatch(%Deposit{account_id: @id1, amount: 500.00})
    wait_for_event(Deposited, fn deposited -> deposited.amount == 500.00 end)
  end

  test "Deposit 003" do 
    :ok = Router.dispatch(%Deposit{account_id: @id1, amount: 500.00})
    wait_for_event(Deposited, fn deposited -> deposited.amount == 500.00 end)
  end


  test "Withdawn amount can't be negative" do
    assert {:error, :amount_is_negative} == Router.dispatch(%Withdraw{account_id: @id1, amount: -500})
  end

  test "Withdraw error, amount can not be zero" do
    assert {:error, :amount_equals_zero} ==  Router.dispatch(%Withdraw{account_id: @id1, amount: 0})
  end

  test "Withdraw" do
    :ok = Router.dispatch(%Withdraw{account_id: @id1, amount: 50.00})
    wait_for_event(Withdrawn, fn balance -> balance.amount == 50.00 end)
  end
  
  test "Insufficient funds" do
    a = Router.dispatch(%Withdraw{account_id: @id1, amount: 100_000_000.00}) 
    b = {:error, :insufficient_funds}    
    assert a == b
  end
  
  test "Send Money 001" do
    :ok = Router.dispatch(%Send{account_id: @id1, amount: 500.00, transfer_id: Nanoid.generate(), to: @id2})
    wait_for_event(Sent, fn x -> x.amount == 500.00 end)
  end
  
  test "Send Money 002" do
    :ok = Router.dispatch(%Send{account_id: @id2, amount: 500.00, transfer_id: Nanoid.generate(), to: @id1})
    wait_for_event(Sent, fn x -> x.amount == 500.00 end)
  end

  test "Send Money Error" do
    a = Router.dispatch(%Send{account_id: @id1, amount: 500_000_000.00, transfer_id: Nanoid.generate(), to: @id2})
    b = {:error, :insufficient_funds}    
    assert a == b
  end

  test "Receive Money" do
    :ok = Router.dispatch(%Receive{account_id: @id2, amount: 1000.00, transfer_id: Nanoid.generate(), from: @id1})
    wait_for_event(Received, fn evt -> evt.amount == 1000.00 end)
  end

  test "we can not add funds to inexistent accounts" do
    assert {:error, :invalid_or_non_active_account} ==  Router.dispatch(%Deposit{amount: 100.00, account_id: "Invalid_ID_002"})
  end

end