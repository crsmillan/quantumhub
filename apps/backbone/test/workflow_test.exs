defmodule Backbone.WorkflowTest do
  use ExUnit.Case, async: true 
  doctest Backbone.Aggregates.Account

  # import Commanded.Assertions.EventAssertions
  alias Remittable.Router
  alias Remittable.Commands.{CreatePartner, CreateUser}
  alias Backbone.Commands.{OpenAccount}


  @partner_id "1ec5f052-f69b-4f4f-bc40-b0a93b47e334"
  @user_id    "d1e9a67b-aea4-4da2-8ad8-c7117fe819ab"
  @account_id "2186947a-ef71-455f-93fe-8962edcddbe6"

  setup_all do
    # create partner
    partner = Router.dispatch %CreatePartner{
      partner_id: @partner_id, 
      name: "Acme Bank",
      bic: "ACMEBANKID"
      } 

    # create user
    user = Router.dispatch %CreateUser{
      user_id: @user_id,
      name: "César Millán",
      partner_id: @partner_id
      }  

    # create account
    account = Router.dispatch %OpenAccount{
      account_id: @account_id, 
      user_id: @user_id, 
      type: "WorkFlow Test"
    }  
    
    # context
    %{
      partner: partner, 
      user:    user, 
      account: account
    }
  end

  test "already in use error", %{} = context do
    assert {:error, :user_already_in_use}     == context.user()
    assert {:error, :partner_already_in_use}  == context.partner()
    assert {:error, :account_already_in_use}  == context.account()

    refute context.user()    == :ok
    refute context.partner() == :ok
    refute context.account() == :ok
  end

  
end