defmodule Api.Graphql.Resolvers.Partner do
  alias Accounting.Repo
  alias Accounting.Schemas.{Partners}

  def get(%{partner_id: id}, _info) do
    partner = Partners
    |> Repo.get(id)
    {:ok, partner}
  end
end