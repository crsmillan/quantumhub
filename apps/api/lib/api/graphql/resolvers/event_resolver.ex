defmodule Api.Graphql.Resolvers.Event do

  def get_events(%{stream_uuid: id}, _info) do
    {:ok, events} = EventStore.read_stream_forward(id)
    {:ok, events}
  end

  def get_events(%{account_id: id}, _args, _info) do
    {:ok, events} = get_events %{stream_uuid: id}, nil
    {:ok, events}
  end
end