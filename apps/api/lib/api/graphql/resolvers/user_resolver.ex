defmodule Api.Graphql.Resolvers.User do
  alias Accounting.Repo
  alias Accounting.Schemas.{Users}
  alias Api.Graphql.Resolvers

  require Logger

  @type parent  :: map
  @type args    :: map
  @type context :: map
  
  @doc "Get User by ID"
  @spec get(args, context) :: {:ok, struct}
  def get(%{user_id: id}, _context) do
    user = Repo.get(Users, id)
    
    {:ok, user}
  end

  @doc "Get a list of Users of given Partner"
  @spec get_all(args, context) :: {:ok, list}
  def get_all(%{partner_id: id}, _) do 
    with {:ok, partner} <- Resolvers.Partner.get(%{partner_id: id}, nil) do
      {:ok, Repo.all Ecto.assoc(partner, :users)}
    end
  end

  @doc "Get a list of Users of nested Partner"
  @spec get_all(parent, args, context) :: {:ok, list}
  def get_all(partner, _, _) do
    with users <- get_all(partner, nil) do
      users
    end
  end

  @doc "Create a new user"
  @spec create(parent, args, context) :: {:ok, map}
  def create(_, args, _) do
    %{user_params: %{name: name}} = args 
    id = Remittable.Server.create_user %{name: name}
    # should return the entire object?
    {:ok, %{user_id: id}}
  end

  @doc "Init a Private Session"
  @spec login(parent, args, context) :: {:ok, map}
  def login(_, args, %{context: context}) do
    Logger.info " #{inspect context}"

    with {:ok, user} <- Api.Management.User.authenticate(args.name, nil),
    {:ok, token, _claims} <- Api.Guardian.encode_and_sign(user) do
      {:ok, %{token: token, user: user}}   
    else
      _ -> {:error, "Invalid Params -> #{inspect args}"}
    end
  end 
  
  @doc "Destroy a Private Session"
  def logout() do
  end

  @doc "Destroy a selected User"
  def delete() do
  end

  @doc "create new account linked to given user"
  def open_account(_parent, _args, %{context: %{current_user: current_user}}) do
    cond do
      # TODO -> first verify the authenticity of token 
      current_user -> 
        Remittable.Server.open_account(%{user_id: current_user.user_id})
        {:ok, %{user_id: current_user.user_id}}
        
      current_user == nil -> 
       {:error, :invalid_token} 
    end

  end

  def edit() do
  end


end