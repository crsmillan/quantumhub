defmodule Api.Graphql.Resolvers.Account do
  alias Accounting.Repo
  alias Accounting.Schemas.{Accounts}
  alias Api.Graphql.Resolvers

  @type parent  :: map
  @type args    :: map
  @type context :: map

  @doc "Get Account by ID"
  @spec get(args, context) :: {:ok, struct}
  def get(%{account_id: id}, _) do
    {:ok, Repo.get(Accounts, id)}
  end

  @doc "Get a list of Accounts of given User using ID "
  @spec get(args, context) :: {:ok, list}
  def get_all(%{user_id: id}, _) when is_binary(id) do 
    with {:ok, user} <- Resolvers.User.get(%{user_id: id}, nil) do
      {:ok, Repo.all Ecto.assoc(user, :accounts)}

      # else condition.
    end
  end
  
  @doc "Get a list of Accounts of given User using token "
  @spec get(args, context) :: {:ok, list}
  def get_all(_args, %{context: %{current_user: current_user}}) do 
    case current_user do
      nil -> {:error, :invalid_token}
      %{} -> get_all(%{user_id: current_user.user_id}, nil)
    end
  end

  @doc "Get a list of Accounts of nested User "
  @spec get_all(parent, args, context) :: {:ok, list}
  def get_all(user, _, _) do
    with accounts <- get_all(user, nil) do
      accounts  
    end
  end

  
end