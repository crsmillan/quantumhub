defmodule Api.Graphql.Pipeline do
  @moduledoc """
    Mechanisim for Absinthe working with Guardian
  """
  use Plug.Builder
  
  plug :set_context
  
  cond do
    Mix.env == :dev or :test  -> 
      plug Absinthe.Plug.GraphiQL, 
      schema: Api.Graphql.Schema
    Mix.env == :prod  -> 
      plug Absinthe.Plug, 
      schema: Api.Graphql.Schema
    true ->
      raise RuntimeError, "Mix.env >> #{inspect Mix.env()}"
  end


  def set_context(conn, _opts) do 
    conn
    |> Absinthe.Plug.put_options(context: build_context(conn))
  end

  defp build_context(conn) do
    with ["Bearer "<>token ] <- get_req_header(conn, "authorization"),
    {:ok, claims}         <- Api.Guardian.decode_and_verify(token),
    {:ok, user}           <- Api.Guardian.resource_from_claims(claims) do
      
      %{current_user: user}
    else
      _ -> %{current_user: nil}
    end
  end

end