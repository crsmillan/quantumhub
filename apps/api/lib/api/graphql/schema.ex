defmodule Api.Graphql.Schema do
  # 3019308
  use Absinthe.Schema
  alias Api.Graphql.Resolvers

  import_types Api.Graphql.Types.Partner
  import_types Api.Graphql.Types.User
  import_types Api.Graphql.Types.Account
  import_types Api.Graphql.Types.Event
  import_types Api.Graphql.Types.Transaction
  import_types Api.Graphql.Types.Operation

  query do

    @desc "Get User by ID"
    field :user, :user do
      arg :user_id, type: non_null(:id)      
      resolve &Resolvers.User.get/2
    end

    @doc "Get all users of specific partner"
    @desc "Get all Users by Partner"
    field :users, list_of(:user) do
      arg :partner_id, type: non_null(:id)
      resolve &Resolvers.User.get_all/2
    end
      
    @doc "Get all accounts of specific user"
    @desc "Get all Accounts by User"
    field :accounts, list_of(:account) do
      arg :user_id, type: :id
      resolve &Resolvers.Account.get_all/2
    end

    @desc "Get Account by ID"
    field :account, :account do
      arg :account_id, type: non_null(:id)
      resolve &Resolvers.Account.get/2
    end

    @desc "Get Balance of an Account"

    @desc "Get Partner by ID"
    field :partner, :partner do
      arg :partner_id, type: non_null(:id)
      resolve &Resolvers.Partner.get/2
    end

    @desc "Get Events of Account"
    field :events, list_of(:event) do
      arg :stream_uuid, type: non_null(:id)
      resolve &Resolvers.Event.get_events/2
    end
  end

  mutation do
    @desc "Create User"
    field :create_user, type: :user do
      arg :user_params, :user_params
      resolve &Resolvers.User.create/3
    end

    @desc "Log-in User"
    field :login_user, type: :user_session do
      arg :name, non_null(:string)
      resolve &Resolvers.User.login/3
    end

    @doc "OpenAccount by User using a token "
    @desc "Open an Account with zero balance"
    field :open_account, type: :user do
      # token as an argument
      resolve &Resolvers.User.open_account/3
    end


    @doc ""
    @desc "Deposit monney into an account"
    field :deposit, type: :transaction do
      #
    end

  end
end