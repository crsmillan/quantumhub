defmodule Api.Graphql.Types.Partner do
  @moduledoc """
    Module to define Partner Object Types
  """

  alias Api.Graphql.Resolvers
  use Absinthe.Schema.Notation
  

  @desc "Partner Description"
  object :partner do
    field :partner_id, non_null(:id)
    field :bic,   :string
    field :name,  :string
    field :users, list_of(:user) do
      resolve &Resolvers.User.get_all/3
    end
  end


end