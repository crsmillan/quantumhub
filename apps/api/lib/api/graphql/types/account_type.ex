defmodule Api.Graphql.Types.Account do
  @moduledoc """
    Module to define Account Object Types
  """
  
  alias Api.Graphql.Resolvers
  use Absinthe.Schema.Notation
  
  @desc "Account Description"
  object :account do
    field :account_id, non_null(:id)
    field :balance,   :float
    field :events, list_of(:event) do
      resolve &Resolvers.Event.get_events/3
    end
  end


  @desc "Account Params"
  input_object :account_params do
    field :user_token, non_null(:string)
  end
  

  
end