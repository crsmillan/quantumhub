defmodule Api.Graphql.Types.Transaction do
	@moduledoc """
    Module to define Transaction Object Types
	"""

	# alias Api.Graphql.Resolvers
	use Absinthe.Schema.Notation
	
	@desc "Transaction Description"
	object :transaction do
		field :operation, type: :operation
	end

end

