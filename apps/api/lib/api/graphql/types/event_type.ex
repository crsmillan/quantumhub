
defmodule Api.Graphql.Types.Event do
  @moduledoc """
    Module to define Event Object Types
  """
  
  # alias Api.Graphql.Resolvers
  use Absinthe.Schema.Notation
  import_types Absinthe.Type.Custom
  
  @desc "Event Description"
  object :event do
    field :event_id,       :id
    field :stream_uuid,    :string,         name: "account_id"
    field :event_type,     :string,         name: "operation"
    field :created_at,     :naive_datetime, name: "date"
    field :stream_version, :integer,        name: "version"
    # field :data, :string
  end

  @desc "Data's Event"
  object :data do
  end

  
end