
defmodule Api.Graphql.Types.User do
  @moduledoc """
    Module to define User Object Types
  """
  
  alias Api.Graphql.Resolvers
  use Absinthe.Schema.Notation
  

  @desc "User"
  object :user do
    field :user_id, non_null(:id)
    field :name, :string
    
    field :accounts, list_of(:account) do
      resolve &Resolvers.Account.get_all/3
    end
  end

  @desc "User Params"
  input_object :user_params do
    field :name, non_null(:string)
  end


  @desc "User Session"
  object :user_session do
    field :token, :string
  end

end