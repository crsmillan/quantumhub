defmodule Api.Router do
  use Plug.Router
  require Logger
  
  plug :match
  plug Plug.Parsers,
    parsers: [
      :urlencoded, 
      :multipart, 
      :json, 
      Absinthe.Plug.Parser
    ],
    pass: ["*/*"],
    json_decoder: Poison

  plug :dispatch
  
  @api_port         Application.get_env(:api, :api_port)
  @stellar_toml_dir Application.get_env(:stellar, :stellar_toml_dir)
  @stellar_toml     "/.well-know/stellar.toml"

  def init(opts) do
    Logger.debug "ApiRouter running  port: #{inspect @api_port}!"
    opts
  end

  forward "/api", to: Api.Graphql.Pipeline

  get @stellar_toml do 
    send_file(conn, 200, @stellar_toml_dir)
  end

  get "home" do
    send_resp(conn, 200, "HTTPS is Working?")
  end

  match _, do: send_resp(conn, 404, "opps, something wrong -> Verify the endpoint")
end
