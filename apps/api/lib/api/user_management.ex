defmodule Api.Management.User do
  alias Accounting.Schemas.Users
  alias Accounting.Repo
  import Ecto.Query
  
  def authenticate(name, _password) do
    query = from u in Users, where: u.name == ^name

    case Repo.one(query) do
      nil -> {:error, "invalid credentials"}  
      user -> verify_password(user)
    end
  end

  defp verify_password(user) do
    {:ok, user}
  end
end