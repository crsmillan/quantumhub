defmodule Api.Cors do
  use Corsica.Router,
    # origins: ["http://localhost", ~r{^https?://(.*\.?)foo\.com$}],
    origins: "*",
    allow_credentials: true,
    max_age: 600

end