defmodule Api.Application do
  @moduledoc false

  require Logger

  use Application
  @api_port Application.get_env(:api, :api_port)

  def start(_type, _args) do

    cowboy_opts =[
      keyfile:  "priv/keys/localhost.key",
      certfile: "priv/keys/localhost.cert",
      otp_app:  :api,
      port:     @api_port
    ]

    children = [
      Plug.Cowboy.child_spec(
        scheme:   :https, 
        plug:     Api.Pipeline, 
        options:  cowboy_opts
      )
    ]

    Logger.debug "API Runnning on #{inspect @api_port} port"
    opts = [strategy: :one_for_one, name: Api.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
