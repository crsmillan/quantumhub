defmodule Api.Guardian do
  alias Accounting.Schemas.{Users}
  use Guardian, otp_app: :api

  def subject_for_token(resource, _claims) do
    sub = to_string(resource.user_id)
    {:ok, sub}
  end

  def subject_for_token(_, _) do
    {:error, :something_went_wrong}
  end

  def resource_from_claims(%{"sub" => user_id}) do
    case Users.get(user_id) do
      nil -> {:error, :user_not_found}
      user -> user
    end
  end

  def resource_from_claims(_claims) do
    {:error, :something_went_wrong}
  end
end