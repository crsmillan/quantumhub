defmodule Api.Pipeline do
  @moduledoc """
    Quantum Api Pipeline Definition
  """
  use Plug.Builder

  plug Plug.Logger
  plug Corsica, origins: "*"
  plug Api.Router


end