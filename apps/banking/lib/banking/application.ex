defmodule Banking.Application do
  use Application
  require Logger

  def start(_type, _args) do
    Logger.debug "Banking is Running" 
    children = [
      
    ]

    opts = [strategy: :one_for_one, name: Banking.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
