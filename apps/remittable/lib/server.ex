defmodule Remittable.Server do
  @moduledoc """
    Remittable Business Logic Core
  """
  use GenServer
  require Logger
  @server :remittable_server

  alias Remittable.Router
  alias Remittable.Commands.{CreatePartner, CreateUser}
  alias Accounting.Repo
  alias Accounting.Schemas.{Accounts}

  # Client API
  def start_link(opts \\ %{}) do
    GenServer.start_link(__MODULE__, opts, name: @server)
  end

  def stop do
    GenServer.cast(@server, :stop)
  end

  def create_user(attrs) when is_map(attrs) do
    cond do
      [:name] 
      |> Enum.all?(&(Map.has_key?(attrs, &1))) -> 
        GenServer.call(@server, {:new_user, attrs})

      true -> 
        raise ArgumentError, "Invalid Data"        
    end
  end

  def create_partner(attrs) when is_map(attrs) do
    cond do
      [:name]
      |> Enum.all?(&(Map.has_key?(attrs, &1))) ->
        GenServer.call(@server, {:new_partnership, attrs})
      
      true -> 
        raise ArgumentError, "Invalid Data"
    end
  end

  def open_account(attrs) when is_map(attrs) do
    cond do
      [:user_id]
      |> Enum.all?(&(Map.has_key?(attrs, &1))) -> 
        GenServer.cast(@server, {:open_account, attrs})

      true -> 
        raise ArgumentError, "Invalid Data"
    end
  end

  def delete_account(account) do
    GenServer.cast(@server, {:delete_account, account})
  end

  def get_balance_of(account) do
    GenServer.call(@server, {:get_balance, account})
  end

  def get_account_info(account) do
    GenServer.call(@server, {:get_account_info, account})
  end

  def get_all_accounts do
    GenServer.call(@server, :get_all_accounts)
  end

  def total_accounts do
    GenServer.call(@server, :count_accounts)
  end

  def get_transactions_of(account) do
    GenServer.call(@server, {:get_transactions_of, account})
  end

  def count_transactions(account) do
    GenServer.call @server, {:count_transactions_of, account}
  end

  def deposit(attrs) when is_map(attrs) do
    cond do
      [:account_id, :amount]
      |> Enum.all?(&(Map.has_key?(attrs, &1))) -> 
        GenServer.cast(@server, {:deposit, attrs})

      true -> 
        raise ArgumentError, "Invalid Data"
    end
  end

  def withdraw(attrs) when is_map(attrs) do
    cond do
      [:account_id, :amount]
      |> Enum.all?(&(Map.has_key?(attrs, &1))) ->
        GenServer.cast(@server, {:withdraw, attrs})

      true -> 
        raise ArgumentError, "Invalid Data"
    end
  end

  def transfer(attrs) when is_map(attrs) do
    cond do
      [:from, :amount, :to]
      |> Enum.all?(&(Map.has_key?(attrs, &1)))  ->  
        GenServer.cast(@server, {:transfer, attrs})
        
      true -> 
        raise ArgumentError, "Invalid Data"
    end
  end

  # Callbacks
  def init(args) do
    Logger.debug "Remittable Server Started on -> (#{inspect self()})"
    {:ok, args}
  end

  def handle_info(msg, state) do
    Logger.info "Remittable Server msg: #{inspect msg}"
    {:noreply, state}
  end

  def handle_call({:new_user, attrs}, _from, state) do
    id = UUID.uuid4
    %CreateUser{
      user_id:      id,
      name:         attrs.name,
      partner_id:   "1ec5f052-f69b-4f4f-bc40-b0a93b47e334" #Acme_Bank Partner
    }
    |> Router.dispatch
    |> case do
      :ok -> Logger.info :user_created
      {:error, err} -> Logger.error err
    end

    {:reply, id, state}
  end

  def handle_call({:new_partnership, attrs}, _from, state) do
    id = UUID.uuid4
    %CreatePartner{
      partner_id:   id,
      name: attrs.name,
      bic:  attrs.bic
    }
    |> Router.dispatch
    |> case do
      :ok -> Logger.info :partnership_created
      {:error, err} -> Logger.error err
    end
    {:reply, id ,state}
  end

  def handle_cast({:open_account, _attrs} = cast, state) do
    GenServer.cast(:backbone_server, cast)
    {:noreply, state}
  end

  def handle_cast({:delete_account, _account} = cast, state) do
    GenServer.cast(:backbone_server,cast)
    {:noreply, state}
  end

  def handle_call({:get_balance, uuid}, _from, state) do
    case get_account(uuid) do
      {:ok, acc} ->    {:reply, acc.balance, state}
      {:error, err} -> {:reply, err, state}
    end
  end

  def handle_call(:get_all_accounts, _from, state) do
    accounts = Repo.all Accounts
    {:reply, accounts, state}
  end

  def handle_call(:count_accounts, _from, state) do
    accounts = Accounts 
    |> Repo.all 
    |> Enum.count
    {:reply, accounts, state}
  end

  def handle_call({:get_account_info, account}, _from, state) do
    account_info = Accounts
    |> Repo.get(account)
    {:reply, account_info, state}
  end

  def handle_call({:get_transactions_of, account}, _from, state) do
    transactions = EventStore.read_stream_forward(account)
    {:reply, transactions, state}
  end

  def handle_call({:count_transactions_of, account}, _from, state) do
    {:ok , list} = EventStore.read_stream_forward(account)
    transactions = Enum.count list
    {:reply, transactions, state}
  end

  def handle_cast({:deposit, _attrs} = cast, state) do
    GenServer.cast(:backbone_server, cast)
    {:noreply, state}
  end

  def handle_cast({:withdraw, _attrs} = cast, state) do
    GenServer.cast(:backbone_server, cast)
    {:noreply, state}
  end

  def handle_cast({:transfer, _attrs} = cast, state) do
    GenServer.cast(:backbone_server, cast)
    {:noreply, state}
  end

  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end

  def terminate(reason, state) do
    Logger.error "Remittable Server Ended --> #{reason}"
    inspect state
    :ok
  end
  
  # # Helpers
  defp get_account(account_uuid) do
    case Repo.get(Accounts, account_uuid) do
      nil ->  {:error, :account_not_found}
      acc ->  {:ok, acc}
    end
  end
end