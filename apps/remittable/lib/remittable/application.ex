defmodule Remittable.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      {Remittable.Server,  %{}},

    ]

    opts = [strategy: :one_for_one, name: Remittable.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
