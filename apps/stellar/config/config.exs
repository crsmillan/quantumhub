use Mix.Config

config :stellar, 
  bridge_sender_port: 8002,
  bridge_listener_port: 8003,
  stellar_toml_dir: "apps/stellar/lib/stellar.toml"

