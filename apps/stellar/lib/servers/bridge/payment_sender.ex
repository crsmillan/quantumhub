defmodule Stellar.Servers.Bridge.PaymentSender do
  use GenServer
  require Logger
  alias Stellar.Operations.Payment.Params, as: PaymentParams

  @server :payment_sender
  @header %{"Content-type" => "application/x-www-form-urlencoded"}
  @port Application.get_env(:stellar, :bridge_sender_port)

  # Client API 
  def start_link(opts \\ :ok) do 
    GenServer.start_link(__MODULE__, opts, name: @server)
  end
  
  def stop do 
    GenServer.cast(@server, :stop)
  end
  
  def sendPayment(%PaymentParams{} = params) do
    [_head | tail] =  Map.to_list(params)
    GenServer.cast(@server, {:send_payment, tail})
  end

  def sendPayment(_)  do
    raise ArgumentError, "Verify Params"
  end
  
  # CallBacks 
  def init(args) do
    Logger.debug "Quantum PaymentSender running!"
    {:ok, args }
  end
 
  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end

  def handle_cast({:send_payment, parameters}, state) do
    parameters
    |> parsing_details
    |> submit_transaction
    |> handle_response
    {:noreply, state}
  end

  def handle_info(msg, state) do
    IO.puts "PaymentSender Message: #{inspect msg}"
    {:noreply, state}
  end

  def terminate(reason, state) do
    Logger.error "PaymentSender Server terminated because of --> #{inspect reason}"
    inspect state
    :ok
  end

  # Helpers 
  defp parsing_details(parameters) do
    url = "http://localhost:#{@port}/payment"
    body = {:form, parameters}
    {url, body}
  end

  defp submit_transaction({url, body})  do
    HTTPoison.post(url, body, @header, [timeout: :infinity, recv_timeout: 50_000])
  end

  defp handle_response({:ok, %HTTPoison.Response{status_code: status, body: body}}) when status == 200 do
    Logger.debug "Payment Succesfully Sent"
    Logger.info body
  end

  defp handle_response({:ok, %HTTPoison.Response{status_code: status, body: body}}) when status == 400 do
    Logger.debug "#{body}"
    send @server, {:status, status}
  end

  defp handle_response({:ok, %HTTPoison.Response{status_code: status, body: body}}) do
    Logger.debug "status: #{status} -- body: #{body}"
    send @server, {:status, status}
  end

  defp handle_response({:ok, %HTTPoison.Response{status_code: status, body: body}}) do
    Logger.debug " #{body}"    
    send @server, {:status, status}
  end

  defp handle_response({:error, %HTTPoison.Error{reason: response}}) do
    Logger.error "Payment with no success -> reason: #{response}" 
  end

end