defmodule Stellar.Servers.Bridge.PaymentListener do
  use Plug.Router
  require Logger

  plug Plug.Logger
  plug :match
  plug Plug.Parsers, parsers: [:urlencoded], pass: ["application/x-www-form-urlencoded"]
  plug :dispatch

  def init(opts) do
    Logger.debug "Quantum PaymentListener running  port: 8003 #{inspect opts}!"
    opts
  end

  post "/receive" do
    Logger.debug "Payment Successfully Received"

    handle_response(conn.params)

    send_resp(conn, 200, "Success!")
  end

  post "/error" do
    body = conn.body_params
    |> handle_response

    send_resp(conn, 200, Poison.encode!(%{response: "#{inspect body}"}))
  end

  match _, do: send_resp(conn, 404, "opps, something wrong -> Verify the endpoint")
  
  # Helpers 

  defp handle_response(body_params) do
    "PaymentListener Message: #{inspect body_params}" |> Logger.info
  end
end