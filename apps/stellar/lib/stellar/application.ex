defmodule Stellar.Application do
  use Application
  alias Stellar.Servers.Bridge.PaymentListener

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    children = [
      {Stellar.Servers.Bridge.PaymentSender, :ok},
      Plug.Cowboy.child_spec(
        scheme: :http, 
        plug: PaymentListener, 
        options: [
          port: Application.get_env(:stellar, :bridge_listener_port)
        ]
      ),
    ]

    opts = [strategy: :one_for_one, name: Stellar.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
