defmodule Stellar.Operations do
  @moduledoc """
    Transactions are made up of a list of operations. 
    Each operation is an individual command that mutates the ledger.

    Operation types:
      -> Create Account
      -> Payment
      -> Path Payment
      -> Manage Offer
      -> Create Passive Offer
      -> Set Options
      -> Change Trust
      -> Allow Trust
      -> Account Merge
      -> Inflation
      -> Manage Data
      -> Bump Sequence
  """

  defmodule Payment do
    @moduledoc """
      Sends an amount in a specific asset to a destination account.
    """
    defmodule Params do
      @keys [
        :destination,
        :amount,
        :memo_type,
        :memo, 
      ]
      @enforce_keys @keys
      defstruct @keys
    end
  end
end