
defmodule TimeFrame do
  defmacro execute(name, units \\ :microsecond, do: yield) do
    quote do
      start = System.monotonic_time(unquote(units))
      result = unquote(yield)
      time_spent = System.monotonic_time(unquote(units)) - start
      IO.puts("Executed #{unquote(name)} in #{time_spent} #{unquote(units)}")
      result
    end
  end
end

params = %Stellar.Operations.Payment.Params{
  amount: 1, 
  destination: "GACKBOFH5C73SN25L3LAUEEVL7XQDU63XUCZLM4LYFEGQPG4KFBDDOZT", 
  memo_type: "text",
  memo: "HelloWorld",
}

list = [
  amount: 1, 
  destination: "GACKBOFH5C73SN25L3LAUEEVL7XQDU63XUCZLM4LYFEGQPG4KFBDDOZT", 
  memo_type: "text",
  memo: "HelloWorld",
]

list2 = [
  amount: 1,
  destination: "540348c6-af3a-47d8-b78b-47e1f2ad4811*quantum.co",
  memo_type: "text",
  memo: "540348c6-af3a-47d8-b78b-47e1f2ad4811"
]

sender = &(for i <- 0..&1, i > 0, do: Stellar.Servers.Bridge.PaymentSender.sendPayment(params))
# sender.(iterations)


System.cmd "chmod", ["+x", "bridge"], cd: "servers/bridge"

import Ecto.Query
alias Backbone.Commands.{OpenAccount} 
alias Remittable.Commands.{CreateUser, AssignAccount, CreatePartner} 
alias Remittable.Router   
alias Remittable.Events.{UserCreated, AccountAssigned}
alias Backbone.Events.{AccountOpened  }
alias Accounting.Repo 
alias Accounting.Schemas.Users, as: User
alias Accounting.Schemas.Partners, as: Partner

Router.dispatch(%CreateUser{
  user_id: "d1e9a67b-aea4-4da2-8ad8-c7117fe819ab",
  name: "César Millán",
  partner_id: "1ec5f052-f69b-4f4f-bc40-b0a93b47e334"
  }
) 

Router.dispatch(%CreatePartner{
  partner_id: "1ec5f052-f69b-4f4f-bc40-b0a93b47e334", 
  name: "Acme Bank",
  bic: "ACMEBANKID"
}) 

Repo.all Ecto.assoc(cesar,  :accounts) # show cesar's accounts
Ecto.build_assoc(cesar, :accounts)  # build_assoc(struct, assoc, attributes \\ %{})  

curl -d "tx=AAAAAOg4wl%2BiLMYSSpybl%2FRPP1NCp2CSK91iCIXPmiLkVsxMAAAAZAAKeHoAAAB1AAAAAQAAAAAAAAAAAAAAAFysMCUAAAAAAAAAAQAAAAAAAAABAAAAAASguKfov7k3XV7WChCVX%2B8B09u9BZWzi8FIaDzcUUIxAAAAAAAAAAAAmJaAAAAAAAAAAAHkVsxMAAAAQLBlUHlxC9Y8ZO9VKRcBqwQz9xKauBt8IkMtXBRLMjsO6YHQ3SuzJiaULEXPhSvQWVNaL8eTBy7khEsQmBA7lQs%3D" -X POST http://localhost:8004/receive

# payment listener
post "/hello" do
  {status, body} =
    case conn.body_params do
      %{"name" => name}   -> {200, say_hello(name)}
      _                   -> {422, missing_name()}
    end

  Logger.info "Body_Params -> #{inspect conn.body_params}"
  send_resp(conn, status, body)
end

defp say_hello(name) do
  Poison.encode!(%{response: "Hello, #{name}!"})
end

defp missing_name do
  Poison.encode!(%{error: "Expected a \"name\" key"})
end