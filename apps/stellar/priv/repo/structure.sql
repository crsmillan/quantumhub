--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: gorp_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gorp_migrations (
    id text NOT NULL,
    applied_at timestamp with time zone
);


--
-- Name: receivedpayment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.receivedpayment (
    id bigint NOT NULL,
    operation_id character varying(255) NOT NULL,
    processed_at timestamp without time zone NOT NULL,
    paging_token character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    transaction_id character varying(64) DEFAULT 'N/A'::character varying
);


--
-- Name: receivedpayment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.receivedpayment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: receivedpayment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.receivedpayment_id_seq OWNED BY public.receivedpayment.id;


--
-- Name: senttransaction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.senttransaction (
    id integer NOT NULL,
    transaction_id character varying(64) NOT NULL,
    status character varying(10) NOT NULL,
    source character varying(56) NOT NULL,
    submitted_at timestamp without time zone NOT NULL,
    succeeded_at timestamp without time zone,
    ledger bigint,
    envelope_xdr text NOT NULL,
    result_xdr character varying(255) DEFAULT NULL::character varying,
    payment_id character varying(255) DEFAULT NULL::character varying
);


--
-- Name: senttransaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.senttransaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: senttransaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.senttransaction_id_seq OWNED BY public.senttransaction.id;


--
-- Name: receivedpayment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.receivedpayment ALTER COLUMN id SET DEFAULT nextval('public.receivedpayment_id_seq'::regclass);


--
-- Name: senttransaction id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.senttransaction ALTER COLUMN id SET DEFAULT nextval('public.senttransaction_id_seq'::regclass);


--
-- Name: gorp_migrations gorp_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gorp_migrations
    ADD CONSTRAINT gorp_migrations_pkey PRIMARY KEY (id);


--
-- Name: senttransaction payment_id_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.senttransaction
    ADD CONSTRAINT payment_id_unique UNIQUE (payment_id);


--
-- Name: receivedpayment receivedpayment_operation_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.receivedpayment
    ADD CONSTRAINT receivedpayment_operation_id_key UNIQUE (operation_id);


--
-- Name: receivedpayment receivedpayment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.receivedpayment
    ADD CONSTRAINT receivedpayment_pkey PRIMARY KEY (id);


--
-- Name: senttransaction senttransaction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.senttransaction
    ADD CONSTRAINT senttransaction_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

