defmodule Stellar.MixProject do
  use Mix.Project

  def project do
    [
      app: :stellar,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [
        # :porcelain
        :logger, 
        :ecto, 
        :postgrex, 
        :cowboy, 
        :plug,
        :plug_cowboy
      ],
      mod: {Stellar.Application, []}
    ]
  end

  defp deps do
    [
      {:database, in_umbrella: true},
      {:httpoison,  ">= 0.0.0"},
      # {:cowboy,     "~> 1.0"},
      {:plug,       "~> 1.7"},
      {:poison,     "~> 3.1"},
      {:plug_cowboy, ">= 0.0.0"},

    ]
  end
end
